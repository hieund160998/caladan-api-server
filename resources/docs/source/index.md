---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)

<!-- END_INFO -->

#Auth Management


<!-- START_c3fa189a6c95ca36ad6ac4791a873d23 -->
## API Login

> Example request:

```bash
curl -X POST \
    "http://localhost/api/login" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4" \
    -d '{"username":"sed","password":"aut"}'

```

```javascript
const url = new URL(
    "http://localhost/api/login"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

let body = {
    "username": "sed",
    "password": "aut"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "status_code": 200,
    "access_token": "15|Pww2bjqtDANWRThIocNnIaYZ0ICmfT39eTfB4yRP",
    "token_type": "Bearer",
    "user_info": {
        "id": 3,
        "name": "test123213123",
        "email": "harum",
        "username": "test_1",
        "created_at": "2022-03-15T16:31:52.000000Z",
        "updated_at": "2022-03-15T16:31:52.000000Z",
        "status": 1
    }
}
```

### HTTP Request
`POST api/login`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `username` | string |  required  | username.
        `password` | string |  required  | password
    
<!-- END_c3fa189a6c95ca36ad6ac4791a873d23 -->

#Construction Management


<!-- START_92ef7348c3d91e762dad2d149050df98 -->
## Display a list of Constructions.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/construction?sort=rerum&page=18&limit=2&order=repellat&name=quaerat&time_from=ea&time_to=sunt&service_id=doloribus&service_type_id=exercitationem" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/construction"
);

let params = {
    "sort": "rerum",
    "page": "18",
    "limit": "2",
    "order": "repellat",
    "name": "quaerat",
    "time_from": "ea",
    "time_to": "sunt",
    "service_id": "doloribus",
    "service_type_id": "exercitationem",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "data": [
        {
            "id": 1,
            "name": "Thang máy",
            "address": "123",
            "service_id": 1,
            "service_type_id": 1,
            "status": 1,
            "person_in_charge": "admin",
            "representative": "admin",
            "representative_tel": "042114122132",
            "representative_mail": "afa@gmail.com",
            "created_by": "admin",
            "updated_by": null,
            "created_at": "2022-02-16T13:50:41.000000Z",
            "updated_at": null,
            "period": "Ky 1",
            "service": {
                "id": 1,
                "name": "Test",
                "type_id": 1,
                "is_active": 1,
                "created_by": "admin",
                "updated_by": "admin",
                "created_at": "2022-02-16T13:49:54.000000Z",
                "updated_at": null
            },
            "service_type": {
                "id": 1,
                "name": "Bao tri",
                "is_active": 1,
                "created_by": "admin",
                "updated_by": null,
                "created_at": "2022-02-18T15:51:54.000000Z",
                "updated_at": null
            }
        }
    ],
    "pagination": {
        "total": 1,
        "current_page": 1,
        "limit": 10
    }
}
```

### HTTP Request
`GET api/construction`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `sort` |  optional  | Field to sort by. Defaults to 'id'.
    `page` |  optional  | Page number. Defaults 1.
    `limit` |  optional  | Maximum number of records. Defaults 10.
    `order` |  optional  | Sort order by. Defaults to 'desc'
    `name` |  optional  | Search by Construction name.
    `time_from` |  optional  | Search for created at from time.  Format: YYYY-MM-DD hh:mm:ss
    `time_to` |  optional  | Search for created at to time. Format: YYYY-MM-DD hh:mm:ss
    `service_id` |  optional  | Search by Construction name.
    `service_type_id` |  optional  | Search by Construction name.

<!-- END_92ef7348c3d91e762dad2d149050df98 -->

<!-- START_f20dc451262f1101d37b5236edf064f7 -->
## Save a new Construction to Database.

> Example request:

```bash
curl -X POST \
    "http://localhost/api/construction" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4" \
    -d '{"name":"delectus","period":"recusandae","address":"dignissimos","service_id":18,"service_type_id":18,"status":6,"person_in_charge":"aut","representative":"minus","representative_tel":"hic","representative_mail":"accusantium"}'

```

```javascript
const url = new URL(
    "http://localhost/api/construction"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

let body = {
    "name": "delectus",
    "period": "recusandae",
    "address": "dignissimos",
    "service_id": 18,
    "service_type_id": 18,
    "status": 6,
    "person_in_charge": "aut",
    "representative": "minus",
    "representative_tel": "hic",
    "representative_mail": "accusantium"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "data": {
        "name": "Bảo trì thang máy",
        "service_type_id": "1",
        "address": "124 Thổ Quan, Đống Đa , HN",
        "service_id": "2",
        "person_in_charge": "Mai Thi Lại",
        "representative": "Trịnh Văn Quyết",
        "representative_mail": "flc@gmail.com",
        "representative_tel": "+84144000222",
        "updated_at": "2022-01-27T07:55:32.000000Z",
        "created_at": "2022-01-27T07:55:32.000000Z",
        "id": 4
    }
}
```

### HTTP Request
`POST api/construction`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | Construction name.
        `period` | string |  required  | Construction period (ky bao tri).
        `address` | string |  required  | Construction address.
        `service_id` | integer |  required  | Service's id.
        `service_type_id` | integer |  required  | Service type's id.
        `status` | integer |  optional  | Construction's status.
        `person_in_charge` | string |  optional  | Construction's person_in_charge.
        `representative` | string |  required  | Representative's name.
        `representative_tel` | string |  required  | Representative's tel.
        `representative_mail` | string |  required  | Representative's mail.
    
<!-- END_f20dc451262f1101d37b5236edf064f7 -->

<!-- START_9dda3ec75a544e2bfc797c309a06ef9f -->
## Display the specified Construction

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/construction/ut" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/construction/ut"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "data": {
        "id": 1,
        "name": "Thang máy",
        "address": "123",
        "service_id": 1,
        "service_type_id": 1,
        "status": 1,
        "person_in_charge": "admin",
        "representative": "admin",
        "representative_tel": "042114122132",
        "representative_mail": "afa@gmail.com",
        "created_by": "admin",
        "updated_by": null,
        "created_at": "2022-02-16T13:50:41.000000Z",
        "updated_at": null,
        "period": "Ky 1",
        "service": {
            "id": 1,
            "name": "Test",
            "type_id": 1,
            "is_active": 1,
            "created_by": "admin",
            "updated_by": "admin",
            "created_at": "2022-02-16T13:49:54.000000Z",
            "updated_at": null
        },
        "service_type": {
            "id": 1,
            "name": "Bao tri",
            "is_active": 1,
            "created_by": "admin",
            "updated_by": null,
            "created_at": "2022-02-18T15:51:54.000000Z",
            "updated_at": null
        }
    }
}
```

### HTTP Request
`GET api/construction/{construction}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `construction` |  required  | The ID of Construction.

<!-- END_9dda3ec75a544e2bfc797c309a06ef9f -->

<!-- START_e884d95fbb621226893a00039f1a95c6 -->
## Update the specified Construction in storage.

> Example request:

```bash
curl -X PUT \
    "http://localhost/api/construction/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4" \
    -d '{"name":"laborum","period":"sunt","address":"cum","service_id":12,"service_type_id":20,"status":5,"person_in_charge":"soluta","representative":"magni","representative_tel":"ipsum","representative_mail":"vel"}'

```

```javascript
const url = new URL(
    "http://localhost/api/construction/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

let body = {
    "name": "laborum",
    "period": "sunt",
    "address": "cum",
    "service_id": 12,
    "service_type_id": 20,
    "status": 5,
    "person_in_charge": "soluta",
    "representative": "magni",
    "representative_tel": "ipsum",
    "representative_mail": "vel"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "message": "Update successful"
}
```

### HTTP Request
`PUT api/construction/{construction}`

`PATCH api/construction/{construction}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `service` |  required  | The ID of updating Construction.
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  optional  | Construction name.
        `period` | string |  required  | Construction period (ky bao tri).
        `address` | string |  optional  | Construction address.
        `service_id` | integer |  optional  | Service's id.
        `service_type_id` | integer |  optional  | Service type's id.
        `status` | integer |  optional  | Construction's status.
        `person_in_charge` | string |  optional  | Construction's person_in_charge.
        `representative` | string |  optional  | Representative's name.
        `representative_tel` | string |  optional  | Representative's tel.
        `representative_mail` | string |  optional  | Representative's mail.
    
<!-- END_e884d95fbb621226893a00039f1a95c6 -->

<!-- START_b9f30a7a82b5fe40c03951848000050c -->
## Remove the specified Construction from storage.

> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/construction/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/construction/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "message": "Delete successful"
}
```

### HTTP Request
`DELETE api/construction/{construction}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `service` |  required  | The ID of removing Construction.

<!-- END_b9f30a7a82b5fe40c03951848000050c -->

#Permission Management


<!-- START_ed8ced07a2186d44fa31e6f39b573d1c -->
## Display a list of permissions.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/permission" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/permission"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "data": [
        {
            "id": 1,
            "title": "Super Admin",
            "key": "*",
            "created_at": "2022-02-16T07:33:55.000000Z",
            "updated_at": null,
            "group_name": "Super Admin",
            "group_id": 0,
            "ability_key": "*"
        },
        {
            "id": 2,
            "title": "Tạo tài khoản mới",
            "key": "user-create",
            "created_at": "2022-02-16T07:33:55.000000Z",
            "updated_at": null,
            "group_name": "Tài khoản",
            "group_id": 1,
            "ability_key": "user-create"
        },
        {
            "id": 3,
            "title": "Reset mật khẩu",
            "key": "password-reset",
            "created_at": "2022-02-16T07:33:55.000000Z",
            "updated_at": null,
            "group_name": "Tài khoản",
            "group_id": 1,
            "ability_key": "password-reset"
        },
        {
            "id": 4,
            "title": "Sửa tài khoản",
            "key": "user-update",
            "created_at": "2022-02-16T07:33:55.000000Z",
            "updated_at": null,
            "group_name": "Tài khoản",
            "group_id": 1,
            "ability_key": "user-update"
        },
        {
            "id": 5,
            "title": "Tạo nhóm người dùng",
            "key": "role-create",
            "created_at": "2022-02-16T07:33:55.000000Z",
            "updated_at": null,
            "group_name": "Tài khoản",
            "group_id": 1,
            "ability_key": "role-create"
        },
        {
            "id": 6,
            "title": "Sửa nhóm người dùng",
            "key": "role-update",
            "created_at": "2022-02-16T07:33:55.000000Z",
            "updated_at": null,
            "group_name": "Tài khoản",
            "group_id": 1,
            "ability_key": "role-update"
        },
        {
            "id": 7,
            "title": "Tạo dịch vụ mới",
            "key": "service-create",
            "created_at": "2022-02-16T07:33:55.000000Z",
            "updated_at": null,
            "group_name": "Quản trị dịch vụ",
            "group_id": 2,
            "ability_key": "service-create"
        },
        {
            "id": 8,
            "title": "Sửa dịch vụ",
            "key": "service-update",
            "created_at": "2022-02-16T07:33:55.000000Z",
            "updated_at": null,
            "group_name": "Quản trị dịch vụ",
            "group_id": 2,
            "ability_key": "service-update"
        },
        {
            "id": 9,
            "title": "Tạo công trình",
            "key": "construction-create",
            "created_at": "2022-02-16T07:33:55.000000Z",
            "updated_at": null,
            "group_name": "Công trình",
            "group_id": 3,
            "ability_key": "construction-create"
        },
        {
            "id": 10,
            "title": "Sửa công trình",
            "key": "construction-update",
            "created_at": "2022-02-16T07:33:55.000000Z",
            "updated_at": null,
            "group_name": "Công trình",
            "group_id": 3,
            "ability_key": "construction-update"
        },
        {
            "id": 11,
            "title": "Thêm lịch sử dịch vụ",
            "key": "service-history-add",
            "created_at": "2022-02-16T07:33:55.000000Z",
            "updated_at": null,
            "group_name": "Công trình",
            "group_id": 3,
            "ability_key": "service-history-add"
        },
        {
            "id": 12,
            "title": "Cập nhật trạng thái",
            "key": "status-update",
            "created_at": "2022-02-16T07:33:55.000000Z",
            "updated_at": null,
            "group_name": "Công trình",
            "group_id": 3,
            "ability_key": "status-update"
        },
        {
            "id": 13,
            "title": "Báo cáo công trình",
            "key": "construction-report",
            "created_at": "2022-02-16T07:33:55.000000Z",
            "updated_at": null,
            "group_name": "Công trình",
            "group_id": 3,
            "ability_key": "construction-report"
        }
    ]
}
```

### HTTP Request
`GET api/permission`


<!-- END_ed8ced07a2186d44fa31e6f39b573d1c -->

#Role Management


<!-- START_01fc43a11672802a440a34de5e43c9ec -->
## Display a list of roles.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/role" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/role"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "data": [
        {
            "id": 1,
            "title": "Admin",
            "created_at": "2022-01-14T16:30:03.000000Z",
            "updated_at": "2022-01-14T16:30:07.000000Z",
            "description": "Full quyền"
        },
        {
            "id": 2,
            "title": "Quản trị",
            "created_at": "2022-01-14T16:30:05.000000Z",
            "updated_at": null,
            "description": ""
        }
    ]
}
```

### HTTP Request
`GET api/role`


<!-- END_01fc43a11672802a440a34de5e43c9ec -->

<!-- START_9da1b300a2c60ef9fb7d7bbbb9f7c300 -->
## Save a new Role to Database.

> Example request:

```bash
curl -X POST \
    "http://localhost/api/role" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4" \
    -d '{"title":"eum","description":"autem","permissions":[]}'

```

```javascript
const url = new URL(
    "http://localhost/api/role"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

let body = {
    "title": "eum",
    "description": "autem",
    "permissions": []
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "data": {
        "title": "HR",
        "description": "human resource",
        "updated_at": "2022-02-18T07:59:37.000000Z",
        "created_at": "2022-02-18T07:59:37.000000Z",
        "id": 5,
        "permissions": [
            {
                "id": 5,
                "role_id": 5,
                "permission_id": 3,
                "created_by": "admin",
                "created_at": "2022-02-18 07:59:37",
                "updated_at": null
            },
            {
                "id": 6,
                "role_id": 5,
                "permission_id": 4,
                "created_by": "admin",
                "created_at": "2022-02-18 07:59:37",
                "updated_at": null
            },
            {
                "id": 7,
                "role_id": 5,
                "permission_id": 5,
                "created_by": "admin",
                "created_at": "2022-02-18 07:59:37",
                "updated_at": null
            }
        ]
    }
}
```

### HTTP Request
`POST api/role`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `title` | string |  required  | Role name.
        `description` | string |  required  | Role description.
        `permissions` | array |  required  | List of permissions ids.
    
<!-- END_9da1b300a2c60ef9fb7d7bbbb9f7c300 -->

<!-- START_36f2eed567a95be3b454a71d1c5a4b97 -->
## Display the specified Role

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/role/minima" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/role/minima"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "data": {
        "id": 13,
        "title": "Kế toán Tổng",
        "created_at": "2022-02-10T10:22:46.000000Z",
        "updated_at": "2022-02-10T10:22:46.000000Z",
        "description": "Trưởng phòng kế toán",
        "permissions": [
            5,
            7,
            6
        ]
    }
}
```

### HTTP Request
`GET api/role/{role}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `role` |  required  | The ID of Role.

<!-- END_36f2eed567a95be3b454a71d1c5a4b97 -->

<!-- START_82f3bd841b4e9f9e752a55da1338ab0c -->
## Update the specified Role in storage.

> Example request:

```bash
curl -X PUT \
    "http://localhost/api/role/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4" \
    -d '{"title":"explicabo","description":"ad","permissions":[]}'

```

```javascript
const url = new URL(
    "http://localhost/api/role/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

let body = {
    "title": "explicabo",
    "description": "ad",
    "permissions": []
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "message": "Update successful"
}
```

### HTTP Request
`PUT api/role/{role}`

`PATCH api/role/{role}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `service` |  required  | The ID of updating Role.
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `title` | string |  required  | Role name.
        `description` | string |  required  | Role description.
        `permissions` | array |  required  | List of permissions ids.
    
<!-- END_82f3bd841b4e9f9e752a55da1338ab0c -->

#Service Management


<!-- START_970efa87e172ba755798eed88226812b -->
## Display a list of Services.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/service?sort=dolores&page=16&limit=4&order=qui&name=cum&time_from=tenetur&time_to=laboriosam" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/service"
);

let params = {
    "sort": "dolores",
    "page": "16",
    "limit": "4",
    "order": "qui",
    "name": "cum",
    "time_from": "tenetur",
    "time_to": "laboriosam",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "data": [
        {
            "id": 3,
            "name": "Cải tạo nhà máy",
            "type_id": 1,
            "is_active": 1,
            "created_by": "admin",
            "updated_by": "admin",
            "created_at": "2022-01-26T14:41:26.000000Z",
            "updated_at": "2022-01-26T14:41:26.000000Z",
            "construction_count": 2,
            "service_type": {
                "id": 1,
                "name": "Bảo trì",
                "is_active": 1,
                "created_by": "admin",
                "updated_by": "admin",
                "created_at": "2022-01-26T21:33:38.000000Z",
                "updated_at": "2022-01-26T21:33:38.000000Z"
            }
        },
        {
            "id": 2,
            "name": "Thang máy",
            "type_id": 2,
            "is_active": 1,
            "created_by": "admin",
            "updated_by": null,
            "created_at": "2022-01-26T14:40:26.000000Z",
            "updated_at": "2022-01-26T14:40:26.000000Z",
            "construction_count": 0,
            "service_type": {
                "id": 2,
                "name": "Sửa chữa",
                "is_active": 1,
                "created_by": "admin",
                "updated_by": "admin",
                "created_at": "2022-01-26T21:33:38.000000Z",
                "updated_at": "2022-01-26T21:33:38.000000Z"
            }
        },
        {
            "id": 1,
            "name": "getall",
            "type_id": 1,
            "is_active": 1,
            "created_by": null,
            "updated_by": null,
            "created_at": "2022-01-26T14:39:24.000000Z",
            "updated_at": "2022-01-26T14:39:24.000000Z",
            "construction_count": 1,
            "service_type": {
                "id": 1,
                "name": "Bảo trì",
                "is_active": 1,
                "created_by": "admin",
                "updated_by": "admin",
                "created_at": "2022-01-26T21:33:38.000000Z",
                "updated_at": "2022-01-26T21:33:38.000000Z"
            }
        }
    ],
    "pagination": {
        "total": 3,
        "current_page": 1,
        "limit": 10
    }
}
```

### HTTP Request
`GET api/service`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `sort` |  optional  | Field to sort by. Defaults to 'id'.
    `page` |  optional  | Page number. Defaults 1.
    `limit` |  optional  | Maximum number of records. Defaults 10.
    `order` |  optional  | Sort order by. Defaults to 'desc'
    `name` |  optional  | Search by Service name.
    `time_from` |  optional  | Search for created at from time.  Format: YYYY-MM-DD hh:mm:ss
    `time_to` |  optional  | Search for created at to time. Format: YYYY-MM-DD hh:mm:ss

<!-- END_970efa87e172ba755798eed88226812b -->

<!-- START_5447fe4970a6e01c8cf38eb6464cceef -->
## Save a new Service to Database.

> Example request:

```bash
curl -X POST \
    "http://localhost/api/service" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4" \
    -d '{"name":"ullam","type_id":19}'

```

```javascript
const url = new URL(
    "http://localhost/api/service"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

let body = {
    "name": "ullam",
    "type_id": 19
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "data": {
        "name": "Repair and Replace",
        "type_id": "2",
        "updated_at": "2022-01-11T06:31:00.000000Z",
        "created_at": "2022-01-11T06:31:00.000000Z",
        "id": 2
    }
}
```

### HTTP Request
`POST api/service`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | Service name.
        `type_id` | integer |  required  | Service type's id.
    
<!-- END_5447fe4970a6e01c8cf38eb6464cceef -->

<!-- START_0069a867ee92ce010687a63e6e686cac -->
## Display the specified Service

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/service/enim" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/service/enim"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "data": {
        "id": 1,
        "name": "Repost",
        "type_id": 1,
        "is_active": 1,
        "created_by": "admin",
        "updated_by": null,
        "created_at": "2022-01-11T04:22:16.000000Z",
        "updated_at": "2022-01-11T04:22:16.000000Z",
        "service_type": {
            "id": 1,
            "name": "Bảo trì",
            "is_active": 1,
            "created_by": "admin",
            "updated_by": null,
            "created_at": "2022-01-11T14:18:48.000000Z",
            "updated_at": "2022-01-11T14:18:50.000000Z"
        }
    }
}
```

### HTTP Request
`GET api/service/{service}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `service` |  required  | The ID of Service.

<!-- END_0069a867ee92ce010687a63e6e686cac -->

<!-- START_d3cdddf820be263f473e0b512d8d02f8 -->
## Update the specified Service in storage.

> Example request:

```bash
curl -X PUT \
    "http://localhost/api/service/sit" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4" \
    -d '{"name":"sit","type_id":8,"is_active":9}'

```

```javascript
const url = new URL(
    "http://localhost/api/service/sit"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

let body = {
    "name": "sit",
    "type_id": 8,
    "is_active": 9
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "message": "Update successful"
}
```

### HTTP Request
`PUT api/service/{service}`

`PATCH api/service/{service}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `service` |  required  | The ID of updating Service.
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | Service name.
        `type_id` | integer |  required  | Service type's id.
        `is_active` | integer |  required  | enum(1,0)
    
<!-- END_d3cdddf820be263f473e0b512d8d02f8 -->

<!-- START_9f5d6d2730461434dc8a14091d3912de -->
## Remove the specified Service from storage.

> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/service/inventore" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/service/inventore"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "message": "Delete successful"
}
```

### HTTP Request
`DELETE api/service/{service}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `service` |  required  | The ID of removing Service.

<!-- END_9f5d6d2730461434dc8a14091d3912de -->

#ServiceType Management


<!-- START_0b91b7510ce4774448923055a4a34097 -->
## Display a list of ServiceTypes.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/service-type" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/service-type"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "data": [
        {
            "id": 1,
            "name": "Bảo trì",
            "is_active": 1,
            "created_by": "admin",
            "updated_by": "admin",
            "created_at": "2022-01-26T21:33:38.000000Z",
            "updated_at": "2022-01-26T21:33:38.000000Z"
        },
        {
            "id": 2,
            "name": "Sửa chữa",
            "is_active": 1,
            "created_by": "admin",
            "updated_by": "admin",
            "created_at": "2022-01-26T21:33:38.000000Z",
            "updated_at": "2022-01-26T21:33:38.000000Z"
        }
    ]
}
```

### HTTP Request
`GET api/service-type`


<!-- END_0b91b7510ce4774448923055a4a34097 -->

#User Management


<!-- START_2b6e5a4b188cb183c7e59558cce36cb6 -->
## Display a listing of Users.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/user?sort=beatae&page=14&limit=6&order=odio&name=illum&username=amet&email=non&tel=17&time_from=fugit&time_to=libero" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/user"
);

let params = {
    "sort": "beatae",
    "page": "14",
    "limit": "6",
    "order": "odio",
    "name": "illum",
    "username": "amet",
    "email": "non",
    "tel": "17",
    "time_from": "fugit",
    "time_to": "libero",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "data": [
        {
            "id": 8,
            "name": "admin1244a",
            "email": "minmid@gmail.com",
            "email_verified_at": null,
            "created_at": "2022-02-09T09:36:10.000000Z",
            "updated_at": "2022-02-09T09:36:10.000000Z",
            "username": "admin1",
            "tel": "0978442111",
            "role_id": 1,
            "status": 1
        },
        {
            "id": 1,
            "name": "HieuNguyen",
            "email": "ndhieu@gmail.com",
            "email_verified_at": null,
            "created_at": "2022-02-09T15:46:33.000000Z",
            "updated_at": null,
            "username": "hieund",
            "tel": "0326778888",
            "role_id": 1,
            "status": 1
        }
    ],
    "pagination": {
        "total": 2,
        "current_page": 1,
        "limit": 10
    }
}
```

### HTTP Request
`GET api/user`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `sort` |  optional  | Field to sort by. Defaults to 'id'.
    `page` |  optional  | Page number. Defaults 1.
    `limit` |  optional  | Maximum number of records. Defaults 10.
    `order` |  optional  | Sort order by. Defaults to 'desc'
    `name` |  optional  | Search by user's name.
    `username` |  optional  | Search by user's username.
    `email` |  optional  | Search by user's email.
    `tel` |  optional  | Search by user's telephone number.
    `time_from` |  optional  | Search for created at from time.  Format: YYYY-MM-DD hh:mm:ss
    `time_to` |  optional  | Search for created at to time. Format: YYYY-MM-DD hh:mm:ss

<!-- END_2b6e5a4b188cb183c7e59558cce36cb6 -->

<!-- START_f0654d3f2fc63c11f5723f233cc53c83 -->
## Save a new User to Database.

> Example request:

```bash
curl -X POST \
    "http://localhost/api/user" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4" \
    -d '{"name":"et","username":"minima","password":"porro","email":"et","tel":"autem"}'

```

```javascript
const url = new URL(
    "http://localhost/api/user"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

let body = {
    "name": "et",
    "username": "minima",
    "password": "porro",
    "email": "et",
    "tel": "autem"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "data": {
        "name": "admin1244a",
        "username": "admin1",
        "email": "minmid@gmail.com",
        "tel": "0978442111",
        "role_id": "1",
        "updated_at": "2022-02-09T09:36:10.000000Z",
        "created_at": "2022-02-09T09:36:10.000000Z",
        "id": 8
    }
}
```
> Example response (200):

```json
{
    "success": false,
    "message": "The username has already been taken."
}
```

### HTTP Request
`POST api/user`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | User's name.
        `username` | string |  required  | User's username.
        `password` | string |  required  | User's username.
        `email` | string |  required  | User's email.
        `tel` | string |  optional  | optional User's email.
    
<!-- END_f0654d3f2fc63c11f5723f233cc53c83 -->

<!-- START_ceec0e0b1d13d731ad96603d26bccc2f -->
## Display the specified resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/user/non" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/user/non"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "data": {
        "id": 1,
        "name": "Hieu Nguyen",
        "email": "ndhieu@gmail.com",
        "email_verified_at": null,
        "created_at": "2022-02-09T15:46:33.000000Z",
        "updated_at": null,
        "username": "hieund",
        "tel": "0326778888",
        "role_id": 1,
        "status": 1
    }
}
```

### HTTP Request
`GET api/user/{user}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `user` |  required  | The ID of user.

<!-- END_ceec0e0b1d13d731ad96603d26bccc2f -->

<!-- START_a4a2abed1e8e8cad5e6a3282812fe3f3 -->
## Update the specified User in database.

> Example request:

```bash
curl -X PUT \
    "http://localhost/api/user/ut" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4" \
    -d '{"name":"aliquam","username":"aperiam","password":"qui","email":"odio","tel":"nesciunt","role_id":7}'

```

```javascript
const url = new URL(
    "http://localhost/api/user/ut"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

let body = {
    "name": "aliquam",
    "username": "aperiam",
    "password": "qui",
    "email": "odio",
    "tel": "nesciunt",
    "role_id": 7
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "message": "Update successful"
}
```

### HTTP Request
`PUT api/user/{user}`

`PATCH api/user/{user}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `user` |  required  | The ID of updating User.
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  optional  | User's name.
        `username` | string |  optional  | User's username.
        `password` | string |  optional  | User's username.
        `email` | string |  optional  | User's email.
        `tel` | string |  optional  | User's tel number.
        `role_id` | integer |  optional  | User's role id.
    
<!-- END_a4a2abed1e8e8cad5e6a3282812fe3f3 -->

<!-- START_4bb7fb4a7501d3cb1ed21acfc3b205a9 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/user/dolores" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/user/dolores"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "success": true,
    "message": "Delete successful"
}
```

### HTTP Request
`DELETE api/user/{user}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `user` |  required  | The ID of updating user.

<!-- END_4bb7fb4a7501d3cb1ed21acfc3b205a9 -->

#general


<!-- START_4dfafe7f87ec132be3c8990dd1fa9078 -->
## Return an empty response simply to trigger the storage of the CSRF cookie in the browser.

> Example request:

```bash
curl -X GET \
    -G "http://localhost/sanctum/csrf-cookie" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/sanctum/csrf-cookie"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`GET sanctum/csrf-cookie`


<!-- END_4dfafe7f87ec132be3c8990dd1fa9078 -->

<!-- START_bb1c3c6798ff2dd1e763f8489dc857c1 -->
## api/list
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/list" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/list"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "CsrfCookieController": [
        "show"
    ],
    "Controller": [
        "routeList",
        "login",
        "loginAdmin"
    ],
    "UserPurchaseController": [
        "getListItem",
        "getUserProfile",
        "getListUserInventory",
        "userPurchase"
    ],
    "ServiceTypeController": [
        "index",
        "create",
        "store",
        "show",
        "edit",
        "update",
        "destroy"
    ],
    "ServiceController": [
        "index",
        "create",
        "store",
        "show",
        "edit",
        "update",
        "destroy"
    ],
    "ConstructionController": [
        "index",
        "create",
        "store",
        "show",
        "edit",
        "update",
        "destroy"
    ],
    "UserController": [
        "index",
        "create",
        "store",
        "show",
        "edit",
        "update",
        "destroy"
    ],
    "RoleController": [
        "index",
        "create",
        "store",
        "show",
        "edit",
        "update",
        "destroy"
    ],
    "PermissionController": [
        "index",
        "create",
        "store",
        "show",
        "edit",
        "update",
        "destroy"
    ]
}
```

### HTTP Request
`GET api/list`


<!-- END_bb1c3c6798ff2dd1e763f8489dc857c1 -->

<!-- START_3a975b26df4276161ca9841e2fb11374 -->
## api/admin-login
> Example request:

```bash
curl -X POST \
    "http://localhost/api/admin-login" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/admin-login"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/admin-login`


<!-- END_3a975b26df4276161ca9841e2fb11374 -->

<!-- START_a437c9e7d81119d48c4cd56ccd236794 -->
## api/item-list
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/item-list" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/item-list"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "msg": "Your session has expired.Please re-authenticate"
}
```

### HTTP Request
`GET api/item-list`


<!-- END_a437c9e7d81119d48c4cd56ccd236794 -->

<!-- START_79dbaea8586a763aad71fbdf53c47222 -->
## api/user-info
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/user-info" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/user-info"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "msg": "Your session has expired.Please re-authenticate"
}
```

### HTTP Request
`GET api/user-info`


<!-- END_79dbaea8586a763aad71fbdf53c47222 -->

<!-- START_5a4bf9617f6c94f71d1c10aa80ea9688 -->
## api/user-inventory
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/user-inventory" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/user-inventory"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (401):

```json
{
    "success": false,
    "msg": "Your session has expired.Please re-authenticate"
}
```

### HTTP Request
`GET api/user-inventory`


<!-- END_5a4bf9617f6c94f71d1c10aa80ea9688 -->

<!-- START_88b5c47903f7890d0dbab9dddbe930e5 -->
## api/user-purchase
> Example request:

```bash
curl -X POST \
    "http://localhost/api/user-purchase" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4"
```

```javascript
const url = new URL(
    "http://localhost/api/user-purchase"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/user-purchase`


<!-- END_88b5c47903f7890d0dbab9dddbe930e5 -->


