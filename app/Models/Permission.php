<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    public $timestamps = true;
    protected $fillable = ['title', 'group_name', 'group_id'];
    protected $hidden = ['deleted_at'];

    public const GROUP_ACCOUNT = 1;
    public const GROUP_SERVICE = 2;
    public const GROUP_CONSTRUCTION = 3;

    public const PERMISSION_CREATE_USER = 'user-create';
    public const PERMISSION_RESET_PASSWORD = 'password-reset';
    public const PERMISSION_UPDATE_USER = 'user-update';
    public const PERMISSION_CREATE_ROLE = 'role-create';
    public const PERMISSION_UPDATE_ROLE = 'role-update';

    public const PERMISSION_CREATE_SERVICE = 'service-create';
    public const PERMISSION_UPDATE_SERVICE = 'service-update';

    public const PERMISSION_CREATE_CONSTRUCTION = 'construction-create';
    public const PERMISSION_UPDATE_CONSTRUCTION = 'construction-update';

    public const PERMISSION_ADD_SERVICE_HISTORY = 'service-history-add';
    public const PERMISSION_UPDATE_STATUS = 'status-update';
    public const PERMISSION_REPORT = 'construction-report';

    public const GROUP_NAME = [
        self::GROUP_ACCOUNT => 'Tài khoản',
        self::GROUP_SERVICE => 'Quản trị dịch vụ',
        self::GROUP_CONSTRUCTION => 'Công trình'
    ];

    public  const PERMISSION_LIST = [
        self::GROUP_ACCOUNT => [
            self::PERMISSION_CREATE_USER => 'Tạo tài khoản mới',
            self::PERMISSION_RESET_PASSWORD => 'Reset mật khẩu',
            self::PERMISSION_UPDATE_USER => 'Sửa tài khoản',
            self::PERMISSION_CREATE_ROLE => 'Tạo nhóm người dùng',
            self::PERMISSION_UPDATE_ROLE => 'Sửa nhóm người dùng'
        ],
        self::GROUP_SERVICE => [
            self::PERMISSION_CREATE_SERVICE => 'Tạo dịch vụ mới',
            self::PERMISSION_UPDATE_SERVICE => 'Sửa dịch vụ'
        ],
        self::GROUP_CONSTRUCTION => [
            self::PERMISSION_CREATE_CONSTRUCTION => 'Tạo công trình',
            self::PERMISSION_UPDATE_CONSTRUCTION => 'Sửa công trình',
            self::PERMISSION_ADD_SERVICE_HISTORY => 'Thêm lịch sử dịch vụ',
            self::PERMISSION_UPDATE_STATUS => 'Cập nhật trạng thái',
            self::PERMISSION_REPORT => 'Báo cáo công trình'
        ]
    ];
}
