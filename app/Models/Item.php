<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    public $timestamps = true;

    public const TYPE_TOOL = 1;
    public const TYPE_ANIMAL = 2;
    public const TYPE_SEED = 3;
    public const TYPE_FACE = 4;
    public const TYPE_FACE_DECOR = 5;
    public const TYPE_HAIR = 6;
    public const TYPE_HAT = 7;
    public const TYPE_SUIT = 8;
    public const TYPE_PRODUCT = 9;

    public const ANIMAL_FOOD_ID = 22;

    public const TYPE_EQUIPABLE = [
        self::TYPE_SUIT,
        self::TYPE_HAT,
        self::TYPE_HAIR,
        self::TYPE_FACE,
        self::TYPE_FACE_DECOR,
    ];

    public const TOOL_ID = [2,3,4,5,34];
}
