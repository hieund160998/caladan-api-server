<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    public $timestamps = true;
    protected $table = 'user_profile';
    protected $fillable = [
        'money', 'suit_id', 'hair_id', 'hat_id', 'face_id', 'face_decor_id', 'user_id', 'owned_slots', 'plot_own'
    ];

    public const START_MONEY = 50000;

    public function hair(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Item::class,'hair_id','id')->where('status',1);
    }

    public function hat(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Item::class,'hat_id','id')->where('status',1);
    }

    public function face(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Item::class,'face_id','id')->where('status',1);
    }

    public function face_decor(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Item::class,'face_decor_id','id')->where('status',1);
    }

    public function suit(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Item::class,'suit_id','id')->where('status',1);
    }
}
