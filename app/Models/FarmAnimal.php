<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class FarmAnimal extends Model
{
    public $timestamps = true;
    protected $table = 'farm_animals';
    protected $fillable = ['user_id', 'item_id', 'product_id', 'start_time', 'growing_time', 'prefab_link', 'last_fed_time', 'dying_time', 'collectable_count', 'last_collect_time', 'next_collect_time', 'status'];

    public function product(): \Illuminate\Database\Eloquent\Relations\belongsTo
    {
        return $this->belongsTo(Item::class,'product_id','id');
    }

    public function item(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Item::class,'item_id','id');
    }
}
