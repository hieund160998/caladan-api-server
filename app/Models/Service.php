<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    public $timestamps = true;
    protected $fillable = ['name', 'type_id', 'is_active', 'created_by', 'updated_by'];

    public function filter($params): array
    {
        $page = !empty($params['page']) ? (int)$params['page'] : 1;
        $limit = !empty($params['limit']) ? (int)$params['limit'] : 10;
        $order = !empty($params['order']) ? $params['order'] : 'DESC';
        $sort = !empty($params['sort']) ? $params['sort'] : 'id';

        $query = $this->where('is_active', 1);

        if (!empty($params['name'])) {
            $query = $query->where('name', '%' . $params['name'] . '%');
        }

        if (!empty($params['type_id'])) {
            $query = $query->where('type_id',  $params['type_id']);
        }

        if (!empty($params['time_from']) && !empty($params['time_to'])) {
            $query = $query->whereBetween('created_at', [$params['time_from'], $params['time_to']]);
        }

        $count = $query->count();

        return [
            'data' => $query->with('serviceType')->withCount('construction')->orderBy($sort, $order)->forPage($page, $limit)->get(),
            'pagination' => [
                'total' => $count,
                'current_page' => $page,
                'limit' => $limit
            ]
        ];
    }

    public function serviceType(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(ServiceType::class, 'type_id', 'id');
    }

    public function construction(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Construction::class, 'service_id', 'id');
    }
}
