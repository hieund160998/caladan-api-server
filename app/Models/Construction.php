<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Construction extends Model
{
    public $timestamps = true;
    public const STT_ACTIVE = 1;
    protected $fillable = ['name', 'address', 'service_id', 'service_type_id', 'status', 'created_by', 'updated_by',
        'representative','representative_tel','representative_mail', 'person_in_charge', 'period'];

    public function filter($params): array
    {
        $page = !empty($params['page']) ? (int)$params['page'] : 1;
        $limit = !empty($params['limit']) ? (int)$params['limit'] : 10;
        $order = !empty($params['order']) ? $params['order'] : 'DESC';
        $sort = !empty($params['sort']) ? $params['sort'] : 'id';

        $query = $this->where('status',  $params['status'] ?? self::STT_ACTIVE);

        if (!empty($params['name'])) {
            $query = $query->where('name', '%' . $params['name'] . '%');
        }

        if (!empty($params['service_id'])) {
            $query = $query->where('service_id',  $params['service_id']);
        }

        if (!empty($params['service_type_id'])) {
            $query = $query->where('service_type_id',  $params['service_type_id']);
        }

        if (!empty($params['time_from']) && !empty($params['time_to'])) {
            $query = $query->whereBetween('created_at', [$params['time_from'], $params['time_to']]);
        }

        $count = $query->count();

        return [
            'data' => $query->with('service')->with('serviceType')->orderBy($sort, $order)->forPage($page, $limit)->get(),
            'pagination' => [
                'total' => $count,
                'current_page' => $page,
                'limit' => $limit
            ]
        ];
    }

    public function serviceType(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(ServiceType::class, 'service_type_id', 'id');
    }

    public function service(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Service::class, 'service_id', 'id');
    }
}
