<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class FarmPlant extends Model
{
    public $timestamps = true;
    protected $table = 'farm_plants';
    protected $fillable = ['user_id', 'item_id', 'plot_id', 'product_id', 'start_time', 'growing_time', 'sprite_link', 'last_fed_time', 'dying_time', 'status'];

    public function product(): \Illuminate\Database\Eloquent\Relations\belongsTo
    {
        return $this->belongsTo(Item::class,'product_id','id');
    }

    public function item(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Item::class,'item_id','id');
    }
}
