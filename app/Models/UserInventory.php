<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserInventory extends Model
{
    public $timestamps = true;
    protected $table = 'user_inventory';
    protected $fillable = ['user_id', 'item_id', 'item_type', 'quantity'];
    protected $hidden = ['status', 'created_at', 'updated_at'];

    public function userProfile(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(UserProfile::class,'user_id','user_id');
    }

    public function item(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Item::class,'item_id','id');
    }
}

