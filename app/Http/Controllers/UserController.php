<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProductRequest;
use App\Models\Item;
use App\Models\UserInventory;
use App\Models\UserProfile;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    protected User $user;
    protected UserInventory $userInventory;
    protected UserProfile $userProfile;

    public function __construct(User $user, UserInventory $userInventory, UserProfile $userProfile)
    {
        $this->user = $user;
        $this->userProfile = $userProfile;
        $this->userInventory = $userInventory;
    }


    /**
     * @group  User Management
     * Display a listing of Users.
     * @headers Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4
     * @queryParam sort Field to sort by. Defaults to 'id'.
     * @queryParam page Page number. Defaults 1.
     * @queryParam limit Maximum number of records. Defaults 10.
     * @queryParam order Sort order by. Defaults to 'desc'
     * @queryParam name Search by user's name.
     * @queryParam username Search by user's username.
     * @queryParam email Search by user's email.
     * @queryParam tel Search by user's telephone number.
     * @queryParam time_from Search for created at from time.  Format: YYYY-MM-DD hh:mm:ss
     * @queryParam time_to  Search for created at to time. Format: YYYY-MM-DD hh:mm:ss
     *
     * @return JsonResponse
     * @response {"success":true,"data":[{"id":8,"name":"admin1244a","email":"minmid@gmail.com","email_verified_at":null,"created_at":"2022-02-09T09:36:10.000000Z","updated_at":"2022-02-09T09:36:10.000000Z","username":"admin1","tel":"0978442111","role_id":1,"status":1},{"id":1,"name":"HieuNguyen","email":"ndhieu@gmail.com","email_verified_at":null,"created_at":"2022-02-09T15:46:33.000000Z","updated_at":null,"username":"hieund","tel":"0326778888","role_id":1,"status":1}],"pagination":{"total":2,"current_page":1,"limit":10}}
     */
    public function index(Request $request)
    {
        //
        $res = $this->user->filter($request->query->all());

        return response()->json(array_merge(['success' => true], $res));
    }


    /**
     * @group  User Management
     * Save a new User to Database.
     * @bodyParam  name string required User's name.
     * @bodyParam  username string required User's username.
     * @bodyParam  password string required User's username.
     * @bodyParam  email string required User's email.
     * @bodyParam  tel string User's phone number.
     * @param Request $request
     * @return JsonResponse
     * @response {"success":true,"data":{"name":"admin1244a","username":"admin1","email":"minmid@gmail.com","tel":"0978442111","role_id":"1","updated_at":"2022-02-09T09:36:10.000000Z","created_at":"2022-02-09T09:36:10.000000Z","id":8}}
     * @response {"success":false,"message":"The username has already been taken."}
     */
    public function store(Request $request): JsonResponse
    {
//        $validator = Validator::make($request->all(), [
//            'username' => 'required|unique:users|max:255',
//            'password' => 'required|max:255|min:5',
//            'name' => 'required|max:255',
//            'email' => 'required|email|unique:users|max:255',
//            'tel' => 'required|max:255|min:8',
//        ]);
//
//        if ($validator->fails()) {
//            return response()->json(['success' => false, 'message' => $validator->errors()->first()]);
//        }

        $password = $request->input('password');
        $request->merge(['password' => Hash::make($request->input('password'))]);
        $data = [];

        DB::beginTransaction();
        try {
            $res = $this->user->create($request->all());

            $this->userProfile->create([
                'user_id' => $res->id,
                'money' => UserProfile::START_MONEY,
                'suit_id' => 34,
                'hair_id' => 0,
                'hat_id' => 0,
                'face_id' => 16,
                'face_decor_id' => 0
            ]);

            foreach (Item::TOOL_ID as $toolId) {
                $this->userInventory->create([
                    'user_id' => $res->id,
                    'item_id' => $toolId,
                    'item_type' => ($toolId == 34) ? Item::TYPE_SUIT : Item::TYPE_TOOL,
                    'quantity' => 1
                ]);
            }

            $data = $res->toArray();
            DB::commit();
        } catch (\Exception $error) {
            DB::rollback();
            return response()->json([
                    'success' => false,
                    'message' => $error->getMessage()
                ]
            );
        }

        return response()->json([
                'success' => true,
                'data' => $data
            ]
        );
    }

    /**
     * @group  User Management
     * Display the specified resource.
     *
     * @urlParam  user required The ID of user.
     * @return JsonResponse
     * @response {"success":true,"data":{"id":1,"name":"Hieu Nguyen","email":"ndhieu@gmail.com","email_verified_at":null,"created_at":"2022-02-09T15:46:33.000000Z","updated_at":null,"username":"hieund","tel":"0326778888","role_id":1,"status":1}}
     */
    public function show($id)
    {
        //
        $res = $this->user->where('id', $id)->with('role')->first();

        return response()->json([
                'success' => true,
                'data' => $res
            ]
        );
    }

    /**
     * @group  User Management
     * Update the specified User in database.
     * @urlParam  user required The ID of updating User.
     * @bodyParam  name string User's name.
     * @bodyParam  username string User's username.
     * @bodyParam  password string User's username.
     * @bodyParam  email string User's email.
     * @bodyParam  tel string User's tel number.
     * @bodyParam  role_id int User's role id.
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     * @response {"success":true,"message":"Update successful"}
     */
    public function update(Request $request, $id)
    {
        $user = $this->user->find($id);

        $request->merge(['password' => Hash::make($request->input('password'))]);

        $res = $this->user->where('id', $id)->update($request->all());

        return response()->json([
            'success' => true,
            'message' => "Update successful"
        ]);
    }

    /**
     * @group  User Management
     * Remove the specified resource from storage.
     *
     * @urlParam  user required The ID of updating user.
     * @param int $id
     * @return JsonResponse
     * @response {"success":true,"message":"Delete successful"}
     */
    public function destroy($id)
    {
        //
        $user = $this->user->destroy($id);

        return response()->json([
            'success' => true,
            'message' => "Delete successful"
        ]);
    }

}
