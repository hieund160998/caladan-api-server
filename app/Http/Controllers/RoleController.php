<?php

namespace App\Http\Controllers;

use App\Models\PermissionRole;
use App\Models\Role;
use App\Models\ServiceType;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RoleController
{
    protected Role $role;

    public function __construct(Role $role)
    {
        $this->role = $role;
    }

    /**
     * @group  Role Management
     * Display a list of roles.
     * @headers Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4
     * @return JsonResponse
     * @response {"success":true,"data":[{"id":1,"title":"Admin","created_at":"2022-01-14T16:30:03.000000Z","updated_at":"2022-01-14T16:30:07.000000Z","description":"Full quy\u1ec1n"},{"id":2,"title":"Qu\u1ea3n tr\u1ecb","created_at":"2022-01-14T16:30:05.000000Z","updated_at":null,"description":""}]}
     */
    public function index()
    {
        //
        $res = $this->role->orderBy('created_at','DESC')->get()->toArray();

        return response()->json(['success' => true,'data' => $res]);
    }


    /**
     * @group  Role Management
     * Save a new Role to Database.
     * @bodyParam  title string required Role name.
     * @bodyParam  description string required Role description.
     * @bodyParam  permissions array required List of permissions ids.
     * @param Request $request
     * @return JsonResponse
     * @response {"success":true,"data":{"title":"HR","description":"human resource","updated_at":"2022-02-18T07:59:37.000000Z","created_at":"2022-02-18T07:59:37.000000Z","id":5,"permissions":[{"id":5,"role_id":5,"permission_id":3,"created_by":"admin","created_at":"2022-02-18 07:59:37","updated_at":null},{"id":6,"role_id":5,"permission_id":4,"created_by":"admin","created_at":"2022-02-18 07:59:37","updated_at":null},{"id":7,"role_id":5,"permission_id":5,"created_by":"admin","created_at":"2022-02-18 07:59:37","updated_at":null}]}}
     */
    public function store(Request $request): JsonResponse
    {
        // TODO: use validator to avoid mass assignment vulnerability
        $res = $this->role->createRole($request->all());

        return response()->json([
                'success' => true,
                'data' => $res
            ]
        );
    }

    /**
     * @group  Role Management
     * Display the specified Role
     *
     * @urlParam  role required The ID of Role.
     * @param $id
     * @return JsonResponse
     * @response {"success":true,"data":{"id":13,"title":"K\u1ebf to\u00e1n T\u1ed5ng","created_at":"2022-02-10T10:22:46.000000Z","updated_at":"2022-02-10T10:22:46.000000Z","description":"Tr\u01b0\u1edfng ph\u00f2ng k\u1ebf to\u00e1n","permissions":[5,7,6]}}
     */
    public function show($id): JsonResponse
    {
        DB::connection()->enableQueryLog();
        //
        $res = $this->role->with('permissionRoles')->find($id);

        $res['permissions'] = array_column($res->permissionRoles->toArray(),'permission_id');
        unset($res->permissionRoles);

        return response()->json([
                'success' => true,
                'data' => $res
            ]
        );
    }

    /**
     * @group  Role Management
     * Update the specified Role in storage.
     * @urlParam  service required The ID of updating Role.
     * @bodyParam  title string required Role name.
     * @bodyParam  description string required Role description.
     * @bodyParam  permissions array required List of permissions ids.
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     * @response {"success":true,"message":"Update successful"}
     */
    public function update(Request $request, $id): JsonResponse
    {
        $res = $this->role->updateRole($request->all(), $id);

        if (!$res) {
            return response()->json([
                'success' => false,
                'message' => "Role's id not found"
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => "Update successful"
        ]);
    }

//    /**
//     * @group  Role Management
//     * Remove the specified Role from storage.
//     * @urlParam  service required The ID of removing Role.
//     * @param int $id
//     * @return JsonResponse
//     * @response {"success":true,"message":"Delete successful"}
//     */
//    public function destroy($id): JsonResponse
//    {
//        //
//        $role = $this->role->destroy($id);
//        PermissionRole::where('role_id', $id)->delete();
//
//        return response()->json([
//            'success' => true,
//            'message' => "Delete successful"
//        ]);
//    }
}
