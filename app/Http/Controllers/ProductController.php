<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProductRequest;
use App\Models\Product;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
/**
 * @authenticated
 *
 * APIs for managing Products
 */
class ProductController extends Controller
{
    protected Product $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * @group  Product Management
     * Display a listing of Products.
     * @headers Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4
     * @queryParam sort Field to sort by. Defaults to 'id'.
     * @queryParam page Page number. Defaults 1.
     * @queryParam limit Maximum number of records. Defaults 10.
     * @queryParam order Sort order by. Defaults to 'desc'
     * @queryParam title Search by product title.
     * @queryParam time_from Search for created at from time.  Format: YYYY-MM-DD hh:mm:ss
     * @queryParam time_to  Search for created at to time. Format: YYYY-MM-DD hh:mm:ss
     * @queryParam price_from Search for price above.
     * @queryParam price_to Search for price below.
     *
     * @return JsonResponse
     * @response {"success":true,"data":[{"id":2,"title":"White Sports Casual T-","slug":"white-sports-casual-t","stock":3,"size":"XL","condition":"hot","price":400,"discount":4,"created_at":"2020-08-14T11:40:21.000000Z","updated_at":"2020-08-14T13:26:01.000000Z"}],"pagination":{"total":1,"current_page":1,"limit":2}}
     */
    public function index(Request $request)
    {
        //
        $res = $this->product->filter($request->query->all());

        return response()->json(array_merge(['success' => true], $res));
    }

    /**
     * @group  Product Management
     * Save a new Product to Database.
     * @bodyParam  title string required Product title.
     * @bodyParam  photo string required Photo url
     * @bodyParam  summary string required Product description
     * @bodyParam  stock int required Product  quantity
     * @bodyParam  condition string required enum('default', 'new', 'hot')
     * @bodyParam  status string required enum('inactive', 'active')
     * @bodyParam  price int required Product price
     * @bodyParam  discount int required Product discount
     * @bodyParam  cat_id int required Product category's id
     * @bodyParam  brand_id int required Product brand's id'
     * @bodyParam  yet_another object required Some object params.
     * @bodyParam  yet_another.name string required Subkey in the object param.
     * @bodyParam  yet_another.tel string required Subtel in the object param.
     * @param StoreProductRequest $request
     * @return JsonResponse
     * @response {"success":true,"data":[{"id":2,"title":"White Sports Casual T-","slug":"white-sports-casual-t","stock":3,"size":"XL","condition":"hot","price":400,"discount":4,"created_at":"2020-08-14T11:40:21.000000Z","updated_at":"2020-08-14T13:26:01.000000Z"}],"pagination":{"total":1,"current_page":1,"limit":2}}
     */
    public function store(StoreProductRequest $request)
    {
        // TODO: use validator to avoid mass assignment vulnerability
        $res = $this->product->create($request->all());

        return response()->json([
                'success' => true,
                'data' => $res
            ]
        );
    }

    /**
     * @group  Product Management
     * Display the specified resource.
     *
     * @urlParam  product required The ID of updating product.
     * @return JsonResponse
     * @response {"success":true,"data":{"id":11,"title":"Vans","summary":"<h1>Nice<\/h1>","photo":"https:\/\/static.nike.com\/a\/images\/t_PDP_864_v1\/f_auto,b_rgb:f5f5f5\/7c2fff38-9f89-40f1-bbcf-ffbfaab5adbc\/wio-8-road-running-shoes-S6jPM3.png","stock":1,"size":"M","condition":"default","status":"active","price":50000,"discount":5,"created_at":"2022-01-04T09:39:47.000000Z","updated_at":"2022-01-04T09:39:47.000000Z"}}
     */
    public function show($id)
    {
        //
        $res = $this->product->find($id);

        return response()->json([
                'success' => true,
                'data' => $res
            ]
        );
    }

    /**
     * @group  Product Management
     * Update the specified Product in storage.
     *
     * @urlParam  product required The ID of updating product.
     * @bodyParam  title string Product title.
     * @bodyParam  photo string Photo url
     * @bodyParam  summary string Product description
     * @bodyParam  stock int Product  quantity
     * @bodyParam  condition string enum('default', 'new', 'hot')
     * @bodyParam  status string enum('inactive', 'active')
     * @bodyParam  price int Product price
     * @bodyParam  discount int Product discount
     * @bodyParam  cat_id int Product category's id
     * @bodyParam  brand_id int Product brand's id'
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     * @response {"success":true,"message":"Update successful"}
     */
    public function update(Request $request, $id)
    {
        $product = $this->product->find($id);
        $res = $this->product->where('id', $id)->update($request->all());

        return response()->json([
            'success' => true,
            'message' => "Update successful"
        ]);
    }

    /**
     * @group  Product Management
     * Remove the specified resource from storage.
     *
     * @urlParam  product required The ID of updating product.
     * @param int $id
     * @return JsonResponse
     * @response {"success":true,"message":"Delete successful"}
     */
    public function destroy($id)
    {
        //
        $product = $this->product->destroy($id);

        return response()->json([
            'success' => true,
            'message' => "Delete successful"
        ]);
    }
}
