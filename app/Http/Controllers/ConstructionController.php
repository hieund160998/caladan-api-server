<?php

namespace App\Http\Controllers;

use App\Models\Construction;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ConstructionController
{
    protected Construction $construction;

    public function __construct(Construction $construction)
    {
        $this->construction = $construction;
    }

    /**
     * @group  Construction Management
     * Display a list of Constructions.
     * @headers Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4
     * @queryParam sort Field to sort by. Defaults to 'id'.
     * @queryParam page Page number. Defaults 1.
     * @queryParam limit Maximum number of records. Defaults 10.
     * @queryParam order Sort order by. Defaults to 'desc'
     * @queryParam name Search by Construction name.
     * @queryParam time_from Search for created at from time.  Format: YYYY-MM-DD hh:mm:ss
     * @queryParam time_to  Search for created at to time. Format: YYYY-MM-DD hh:mm:ss
     * @queryParam service_id Search by Construction name.
     * @queryParam service_type_id Search by Construction name.
     * @return JsonResponse
     * @response {"success":true,"data":[{"id":1,"name":"Thang m\u00e1y","address":"123","service_id":1,"service_type_id":1,"status":1,"person_in_charge":"admin","representative":"admin","representative_tel":"042114122132","representative_mail":"afa@gmail.com","created_by":"admin","updated_by":null,"created_at":"2022-02-16T13:50:41.000000Z","updated_at":null,"period":"Ky 1","service":{"id":1,"name":"Test","type_id":1,"is_active":1,"created_by":"admin","updated_by":"admin","created_at":"2022-02-16T13:49:54.000000Z","updated_at":null},"service_type":{"id":1,"name":"Bao tri","is_active":1,"created_by":"admin","updated_by":null,"created_at":"2022-02-18T15:51:54.000000Z","updated_at":null}}],"pagination":{"total":1,"current_page":1,"limit":10}}
     */
    public function index(Request $request)
    {
        //
        $res = $this->construction->filter($request->query->all());

        return response()->json(array_merge(['success' => true], $res));
    }

    /**
     * @group  Construction Management
     * Save a new Construction to Database.
     * @bodyParam  name string required Construction name.
     * @bodyParam  period string required Construction period (ky bao tri).
     * @bodyParam  address string required Construction address.
     * @bodyParam  service_id int required Service's id.
     * @bodyParam  service_type_id int required Service type's id.
     * @bodyParam  status int Construction's status.
     * @bodyParam  person_in_charge string Construction's person_in_charge.
     * @bodyParam  representative string required Representative's name.
     * @bodyParam  representative_tel string required Representative's tel.
     * @bodyParam  representative_mail string required Representative's mail.
     * @param Request $request
     * @return JsonResponse
     * @response {"success":true,"data":{"name":"B\u1ea3o tr\u00ec thang m\u00e1y","service_type_id":"1","address":"124 Th\u1ed5 Quan, \u0110\u1ed1ng \u0110a , HN","service_id":"2","person_in_charge":"Mai Thi L\u1ea1i","representative":"Tr\u1ecbnh V\u0103n Quy\u1ebft","representative_mail":"flc@gmail.com","representative_tel":"+84144000222","updated_at":"2022-01-27T07:55:32.000000Z","created_at":"2022-01-27T07:55:32.000000Z","id":4}}
     */
    public function store(Request $request): JsonResponse
    {
        // TODO: use validator to avoid mass assignment vulnerability
        $res = $this->construction->create($request->all());

        return response()->json([
                'success' => true,
                'data' => $res
            ]
        );
    }

    /**
     * @group  Construction Management
     * Display the specified Construction
     *
     * @urlParam  construction required The ID of Construction.
     * @param $id
     * @return JsonResponse
     * @response {"success":true,"data":{"id":1,"name":"Thang m\u00e1y","address":"123","service_id":1,"service_type_id":1,"status":1,"person_in_charge":"admin","representative":"admin","representative_tel":"042114122132","representative_mail":"afa@gmail.com","created_by":"admin","updated_by":null,"created_at":"2022-02-16T13:50:41.000000Z","updated_at":null,"period":"Ky 1","service":{"id":1,"name":"Test","type_id":1,"is_active":1,"created_by":"admin","updated_by":"admin","created_at":"2022-02-16T13:49:54.000000Z","updated_at":null},"service_type":{"id":1,"name":"Bao tri","is_active":1,"created_by":"admin","updated_by":null,"created_at":"2022-02-18T15:51:54.000000Z","updated_at":null}}}
     */
    public function show($id): JsonResponse
    {
        //
        $res = $this->construction->with('service')->with('serviceType')->find($id);

        return response()->json([
                'success' => true,
                'data' => $res
            ]
        );
    }

    /**
     * @group  Construction Management
     * Update the specified Construction in storage.
     * @urlParam  service required The ID of updating Construction.
     * @bodyParam  name string Construction name.
     * @bodyParam  period string required Construction period (ky bao tri).
     * @bodyParam  address string Construction address.
     * @bodyParam  service_id int Service's id.
     * @bodyParam  service_type_id int Service type's id.
     * @bodyParam  status int Construction's status.
     * @bodyParam  person_in_charge string Construction's person_in_charge.
     * @bodyParam  representative string Representative's name.
     * @bodyParam  representative_tel string Representative's tel.
     * @bodyParam  representative_mail string Representative's mail.
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     * @response {"success":true,"message":"Update successful"}
     */
    public function update(Request $request, $id): JsonResponse
    {
        $res = $this->construction->where('id', $id)->update($request->all());

        return response()->json([
            'success' => true,
            'message' => "Update successful"
        ]);
    }

    /**
     * @group  Construction Management
     * Remove the specified Construction from storage.
     * @urlParam  service required The ID of removing Construction.
     * @param int $id
     * @return JsonResponse
     * @response {"success":true,"message":"Delete successful"}
     */
    public function destroy($id): JsonResponse
    {
        //
        $Construction = $this->construction->destroy($id);

        return response()->json([
            'success' => true,
            'message' => "Delete successful"
        ]);
    }
}
