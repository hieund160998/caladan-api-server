<?php

namespace App\Http\Controllers;

use App\Models\FarmAnimal;
use App\Models\FarmPlant;
use App\Models\Item;
use App\Models\UserInventory;
use App\Models\UserProfile;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class FarmInventoryController  extends Controller
{
    protected User $user;
    protected UserProfile $userProfile;
    protected UserInventory $userInventory;
    protected Item $item;
    protected FarmAnimal $farmAnimal;
    protected FarmPlant $farmPlant;

    public function __construct(User $user, UserProfile $userProfile, UserInventory $userInventory, Item $item, FarmAnimal $farmAnimal, FarmPlant $farmPlant)
    {
        $this->user = $user;
        $this->userProfile = $userProfile;
        $this->userInventory = $userInventory;
        $this->item = $item;
        $this->farmAnimal = $farmAnimal;
        $this->farmPlant = $farmPlant;
    }

    public function getListPlant(): JsonResponse
    {
        $res = $this->farmPlant->where('user_id', Auth::user()->id)->with('item')->with('product')->get()->toArray();

        return response()->json(['success' => true,'data' => $res]);
    }

    public function plantWater(Request $request): JsonResponse
    {
        $request->validate([
            'plot_id' => 'required',
            'seed_id' => 'required|exists:items,id',
        ]);

        $farmPlant = $this->farmPlant->where('user_id', Auth::user()->id)->where('plot_id', $request->input('plot_id'))->first();
        if (!empty($farmPlant)) {
            $farmPlant->last_fed_time = Carbon::now()->subMinutes(1);
            $farmPlant->save();
        } else {
            return response()->json(['success' => false, 'message' => 'Plant not found!']);
        }
        return response()->json(['success' => true, 'message' => 'Plant watered successfully']);
    }

    public function plantRemove(Request $request): JsonResponse
    {
        $request->validate([
            'plot_id' => 'required',
            'seed_id' => 'required|exists:items,id',
        ]);

        $farmPlant = $this->farmPlant->where('user_id', Auth::user()->id)->where('plot_id', $request->input('plot_id'))->first();
        if (!empty($farmPlant)) {
            $this->farmPlant->where('id',$farmPlant->id)->delete();
        } else {
            return response()->json(['success' => false, 'message' => 'Plant not found!']);
        }
        return response()->json(['success' => true, 'message' => 'Plant remove successfully']);
    }

    public function plantHarvest(Request $request): JsonResponse
    {
        $request->validate([
            'plot_id' => 'required',
            'seed_id' => 'required|exists:items,id',
        ]);

        $farmPlant = $this->farmPlant->where('user_id', Auth::user()->id)->where('plot_id', $request->input('plot_id'))->first();
        if (!empty($farmPlant) && Carbon::now()->subMinutes(1) > Carbon::parse($farmPlant->start_time)->addSecond($farmPlant->growing_time)) {
                $userInventory = $this->userInventory->where('user_id', Auth::user()->id)->where('item_id', $farmPlant->product_id)->first();
                if (empty($userInventory)) {
                    $this->userInventory->create([
                        'user_id' => Auth::user()->id,
                        'item_id' => $farmPlant->product_id,
                        'item_type' => $farmPlant->product->item_type,
                        'quantity' => 1
                    ]);
                } else {
                    $userInventory->quantity += 1;
                    $userInventory->save();
                }
                $this->farmPlant->where('id',$farmPlant->id)->delete();

        } else {
            return response()->json(['success' => false, 'message' => 'Plant not found!']);
        }
        return response()->json(['success' => true, 'message' => 'Plant remove successfully']);
    }

    public function plantSeed(Request $request): JsonResponse
    {
        $request->validate([
            'plot_id' => 'required',
            'seed_id' => 'required|exists:items,id',
        ]);

        $seedInventory = $this->userInventory->where('user_id', Auth::user()->id)->where('item_id', $request->input('seed_id'))->first();
        if (empty($seedInventory)) {
            return response()->json(['success' => false, 'message' => 'Out of seed!']);
        }

        $seed = $this->item->find($request->input('seed_id'));

        $farmPlant = $this->farmPlant->where('user_id', Auth::user()->id)->where('plot_id', $request->input('plot_id'))->first();
        if (!empty($farmPlant)) {
            $farmPlant->item_id = $seed->id;
            $farmPlant->sprite_link = $seed->prefab_link;
            $farmPlant->last_fed_time = Carbon::now()->subMinutes(1);
            $farmPlant->start_time = Carbon::now()->subMinutes(1);
            $farmPlant->growing_time = $seed->time_to_grow;
            $farmPlant->dying_time = $seed->time_to_live;
            $farmPlant->save();
        } else {
            $this->farmPlant->create([
                'user_id' => Auth::user()->id,
                'plot_id' => $request->input('plot_id'),
                'item_id' => $seed->id,
                'product_id' => $seed->product_id,
                'sprite_link' => $seed->prefab_link,
                'start_time' => Carbon::now()->subMinutes(1),
                'growing_time' => $seed->time_to_grow,
                'last_fed_time' => Carbon::now()->subMinutes(1),
                'dying_time' => $seed->time_to_live,
            ]);
        }

        if ($seedInventory->quantity <= 1) {
            $seedInventory->delete();
        }
        else {
            $seedInventory->quantity -= 1;
            $seedInventory->save();
        }

        return response()->json(['success' => true, 'message' => 'Plant successfully']);
    }

    public function getListAnimal(): JsonResponse
    {
        $res = $this->farmAnimal->where('user_id', Auth::user()->id)->with('item')->with('product')->get();

        foreach ($res as $index => $animal) {
            if (Carbon::now()->subMinutes(1) > Carbon::parse($animal->last_fed_time)->addSecond($animal->dying_time)) {
                unset($res[$index]);
                $this->farmAnimal->where('id',$animal->id)->delete();
            }
        }

        return response()->json(['success' => true,'data' => $res]);
    }

    public function collectAnimalProduct(Request $request): JsonResponse
    {
        $request->validate([
            'animal_id' => 'required'
        ]);

        $animal = $this->farmAnimal->where('user_id', Auth::user()->id)->where('id', $request->input('animal_id'))->with('product')->first();
        if (empty($animal) || (Carbon::now()->subMinutes(1) > Carbon::parse($animal->last_fed_time)->addSecond($animal->dying_time))) {
            return response()->json(['success' => false, 'message' => 'Invalid animal!']);
        }

        if (Carbon::now()->subMinutes(1) > Carbon::parse($animal->start_time)->addSecond($animal->growing_time)) {
            $userInventory = $this->userInventory->where('user_id', Auth::user()->id)->where('item_id', $animal->product_id)->first();
            if (empty($userInventory)) {
                $this->userInventory->create([
                    'user_id' => Auth::user()->id,
                    'item_id' => $animal->product_id,
                    'item_type' => $animal->product->item_type,
                    'quantity' => $animal->collectable_count
                ]);
            } else {
                $userInventory->quantity += $animal->collectable_count;
                $userInventory->save();
            }
            $this->farmAnimal->where('id',$animal->id)->delete();
        }

        $res =  $this->userInventory->where('user_id', Auth::user()->id)->with('item')->orderBy('item_type')->orderBy('id')->get();
        return response()->json(['success' => true, 'data' => $res]);
    }

    public function feedAnimal(Request $request): JsonResponse
    {
        $request->validate([
            'animal_id' => 'required'
        ]);

        $farmAnimal = $this->farmAnimal->where('user_id', Auth::user()->id)->where('id', $request->input('animal_id'))->first();
        if (empty($farmAnimal)) {
            return response()->json(['success' => false, 'message' => 'Invalid animal!']);
        }

        $animalFood = $this->userInventory->where('user_id', Auth::user()->id)->where('item_id', Item::ANIMAL_FOOD_ID)->first();
        if (empty($animalFood)) {
            return response()->json(['success' => false, 'message' => 'Animal Food not found!']);
        }

        $farmAnimal->last_fed_time = Carbon::now()->subMinutes(1);;
        $farmAnimal->save();

        if ($animalFood->quantity == 1) {
            $animalFood->delete();
        }
        else {
            $animalFood->quantity -= 1;
            $animalFood->save();
        }

        return response()->json(['success' => true, 'message' => 'Fed successfully']);
    }
}
