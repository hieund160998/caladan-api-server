<?php

namespace App\Http\Controllers;

use App\Models\Service;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ServiceController
{
    protected Service $service;

    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * @group  Service Management
     * Display a list of Services.
     * @headers Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4
     * @queryParam sort Field to sort by. Defaults to 'id'.
     * @queryParam page Page number. Defaults 1.
     * @queryParam limit Maximum number of records. Defaults 10.
     * @queryParam order Sort order by. Defaults to 'desc'
     * @queryParam name Search by Service name.
     * @queryParam time_from Search for created at from time.  Format: YYYY-MM-DD hh:mm:ss
     * @queryParam time_to  Search for created at to time. Format: YYYY-MM-DD hh:mm:ss
     * @return JsonResponse
     * @response {"success":true,"data":[{"id":3,"name":"C\u1ea3i t\u1ea1o nh\u00e0 m\u00e1y","type_id":1,"is_active":1,"created_by":"admin","updated_by":"admin","created_at":"2022-01-26T14:41:26.000000Z","updated_at":"2022-01-26T14:41:26.000000Z","construction_count":2,"service_type":{"id":1,"name":"B\u1ea3o tr\u00ec","is_active":1,"created_by":"admin","updated_by":"admin","created_at":"2022-01-26T21:33:38.000000Z","updated_at":"2022-01-26T21:33:38.000000Z"}},{"id":2,"name":"Thang m\u00e1y","type_id":2,"is_active":1,"created_by":"admin","updated_by":null,"created_at":"2022-01-26T14:40:26.000000Z","updated_at":"2022-01-26T14:40:26.000000Z","construction_count":0,"service_type":{"id":2,"name":"S\u1eeda ch\u1eefa","is_active":1,"created_by":"admin","updated_by":"admin","created_at":"2022-01-26T21:33:38.000000Z","updated_at":"2022-01-26T21:33:38.000000Z"}},{"id":1,"name":"getall","type_id":1,"is_active":1,"created_by":null,"updated_by":null,"created_at":"2022-01-26T14:39:24.000000Z","updated_at":"2022-01-26T14:39:24.000000Z","construction_count":1,"service_type":{"id":1,"name":"B\u1ea3o tr\u00ec","is_active":1,"created_by":"admin","updated_by":"admin","created_at":"2022-01-26T21:33:38.000000Z","updated_at":"2022-01-26T21:33:38.000000Z"}}],"pagination":{"total":3,"current_page":1,"limit":10}}
     */
    public function index(Request $request)
    {
        //
        $res = $this->service->filter($request->query->all());

        return response()->json(array_merge(['success' => true], $res));
    }

    /**
     * @group  Service Management
     * Save a new Service to Database.
     * @bodyParam  name string required Service name.
     * @bodyParam  type_id int required Service type's id.
     * @param Request $request
     * @return JsonResponse
     * @response {"success":true,"data":{"name":"Repair and Replace","type_id":"2","updated_at":"2022-01-11T06:31:00.000000Z","created_at":"2022-01-11T06:31:00.000000Z","id":2}}
     */
    public function store(Request $request)
    {
        // TODO: use validator to avoid mass assignment vulnerability
        $res = $this->service->create($request->all());

        return response()->json([
                'success' => true,
                'data' => $res
            ]
        );
    }

    /**
     * @group  Service Management
     * Display the specified Service
     *
     * @urlParam  service required The ID of Service.
     * @param $id
     * @return JsonResponse
     * @response {"success":true,"data":{"id":1,"name":"Repost","type_id":1,"is_active":1,"created_by":"admin","updated_by":null,"created_at":"2022-01-11T04:22:16.000000Z","updated_at":"2022-01-11T04:22:16.000000Z","service_type":{"id":1,"name":"B\u1ea3o tr\u00ec","is_active":1,"created_by":"admin","updated_by":null,"created_at":"2022-01-11T14:18:48.000000Z","updated_at":"2022-01-11T14:18:50.000000Z"}}}
     */
    public function show($id): JsonResponse
    {
        //
        $res = $this->service->with('serviceType')->find($id);

        return response()->json([
                'success' => true,
                'data' => $res
            ]
        );
    }

    /**
     * @group  Service Management
     * Update the specified Service in storage.
     * @urlParam  service required The ID of updating Service.
     * @bodyParam  name string required Service name.
     * @bodyParam  type_id int required Service type's id.
     * @bodyParam  is_active int required enum(1,0)
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     * @response {"success":true,"message":"Update successful"}
     */
    public function update(Request $request, $id): JsonResponse
    {
        $res = $this->service->where('id', $id)->update($request->all());

        return response()->json([
            'success' => true,
            'message' => "Update successful"
        ]);
    }

    /**
     * @group  Service Management
     * Remove the specified Service from storage.
     * @urlParam  service required The ID of removing Service.
     * @param int $id
     * @return JsonResponse
     * @response {"success":true,"message":"Delete successful"}
     */
    public function destroy($id): JsonResponse
    {
        //
        $Service = $this->service->destroy($id);

        return response()->json([
            'success' => true,
            'message' => "Delete successful"
        ]);
    }
}
