<?php

namespace App\Http\Controllers;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Http\JsonResponse;

class PermissionController
{
    protected Permission $permission;

    public function __construct(Permission $permission)
    {
        $this->permission = $permission;
    }

    /**
     * @group  Permission Management
     * Display a list of permissions.
     * @headers Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4
     * @return JsonResponse
     * @response {"success":true,"data":[{"id":1,"title":"Super Admin","key":"*","created_at":"2022-02-16T07:33:55.000000Z","updated_at":null,"group_name":"Super Admin","group_id":0,"ability_key":"*"},{"id":2,"title":"T\u1ea1o t\u00e0i kho\u1ea3n m\u1edbi","key":"user-create","created_at":"2022-02-16T07:33:55.000000Z","updated_at":null,"group_name":"T\u00e0i kho\u1ea3n","group_id":1,"ability_key":"user-create"},{"id":3,"title":"Reset m\u1eadt kh\u1ea9u","key":"password-reset","created_at":"2022-02-16T07:33:55.000000Z","updated_at":null,"group_name":"T\u00e0i kho\u1ea3n","group_id":1,"ability_key":"password-reset"},{"id":4,"title":"S\u1eeda t\u00e0i kho\u1ea3n","key":"user-update","created_at":"2022-02-16T07:33:55.000000Z","updated_at":null,"group_name":"T\u00e0i kho\u1ea3n","group_id":1,"ability_key":"user-update"},{"id":5,"title":"T\u1ea1o nh\u00f3m ng\u01b0\u1eddi d\u00f9ng","key":"role-create","created_at":"2022-02-16T07:33:55.000000Z","updated_at":null,"group_name":"T\u00e0i kho\u1ea3n","group_id":1,"ability_key":"role-create"},{"id":6,"title":"S\u1eeda nh\u00f3m ng\u01b0\u1eddi d\u00f9ng","key":"role-update","created_at":"2022-02-16T07:33:55.000000Z","updated_at":null,"group_name":"T\u00e0i kho\u1ea3n","group_id":1,"ability_key":"role-update"},{"id":7,"title":"T\u1ea1o d\u1ecbch v\u1ee5 m\u1edbi","key":"service-create","created_at":"2022-02-16T07:33:55.000000Z","updated_at":null,"group_name":"Qu\u1ea3n tr\u1ecb d\u1ecbch v\u1ee5","group_id":2,"ability_key":"service-create"},{"id":8,"title":"S\u1eeda d\u1ecbch v\u1ee5","key":"service-update","created_at":"2022-02-16T07:33:55.000000Z","updated_at":null,"group_name":"Qu\u1ea3n tr\u1ecb d\u1ecbch v\u1ee5","group_id":2,"ability_key":"service-update"},{"id":9,"title":"T\u1ea1o c\u00f4ng tr\u00ecnh","key":"construction-create","created_at":"2022-02-16T07:33:55.000000Z","updated_at":null,"group_name":"C\u00f4ng tr\u00ecnh","group_id":3,"ability_key":"construction-create"},{"id":10,"title":"S\u1eeda c\u00f4ng tr\u00ecnh","key":"construction-update","created_at":"2022-02-16T07:33:55.000000Z","updated_at":null,"group_name":"C\u00f4ng tr\u00ecnh","group_id":3,"ability_key":"construction-update"},{"id":11,"title":"Th\u00eam l\u1ecbch s\u1eed d\u1ecbch v\u1ee5","key":"service-history-add","created_at":"2022-02-16T07:33:55.000000Z","updated_at":null,"group_name":"C\u00f4ng tr\u00ecnh","group_id":3,"ability_key":"service-history-add"},{"id":12,"title":"C\u1eadp nh\u1eadt tr\u1ea1ng th\u00e1i","key":"status-update","created_at":"2022-02-16T07:33:55.000000Z","updated_at":null,"group_name":"C\u00f4ng tr\u00ecnh","group_id":3,"ability_key":"status-update"},{"id":13,"title":"B\u00e1o c\u00e1o c\u00f4ng tr\u00ecnh","key":"construction-report","created_at":"2022-02-16T07:33:55.000000Z","updated_at":null,"group_name":"C\u00f4ng tr\u00ecnh","group_id":3,"ability_key":"construction-report"}]}
     */
    public function index()
    {
        //
        $res = $this->permission->all()->toArray();

        return response()->json(['success' => true,'data' => $res]);
    }
}
