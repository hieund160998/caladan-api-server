<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    protected Category $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    /**
     * @group  Category Management
     * Display a listing of Categories.
     * @headers Authorization: Bearer 10|wrpJyOOlFaGAbvXyOsSvHJQbpYmP0HiPi2KVMck4
     * @queryParam sort Field to sort by. Defaults to 'id'.
     * @queryParam page Page number. Defaults 1.
     * @queryParam limit Maximum number of records. Defaults 10.
     * @queryParam order Sort order by. Defaults to 'desc'
     * @queryParam title Search by Category title.
     * @queryParam time_from Search for created at from time.  Format: YYYY-MM-DD hh:mm:ss
     * @queryParam time_to  Search for created at to time. Format: YYYY-MM-DD hh:mm:ss
     * @return JsonResponse
     * @response {"success":true,"data":[{"id":1,"title":"Men's Fashion","summary":null,"photo":"\/storage\/photos\/1\/Category\/mini-banner1.jpg","is_parent":1,"parent_id":null,"added_by":null,"status":"active","created_at":"2020-08-14T11:26:15.000000Z","updated_at":"2020-08-14T11:26:15.000000Z"},{"id":2,"title":"Women's Fashion","summary":null,"photo":"\/storage\/photos\/1\/Category\/mini-banner2.jpg","is_parent":1,"parent_id":null,"added_by":null,"status":"active","created_at":"2020-08-14T11:26:40.000000Z","updated_at":"2020-08-14T11:26:40.000000Z"}],"pagination":{"total":7,"current_page":1,"limit":2}}
     */
    public function index(Request $request)
    {
        //
        $res = $this->category->filter($request->query->all());

        return response()->json(array_merge(['success' => true], $res));
    }

    /**
     * @group  Category Management
     * Save a new Category to Database.
     * @bodyParam  title string required Category title.
     * @bodyParam  photo string required Photo url
     * @bodyParam  summary string required Category description
     * @bodyParam  status string required enum('inactive', 'active')
     * @bodyParam  is_parent int required enum(1,0)
     * @bodyParam  parent_id int Parent category's id
     * @param Request $request
     * @return JsonResponse
     * @response {"success":true,"data":[{"id":2,"title":"White Sports Casual T-","slug":"white-sports-casual-t","stock":3,"size":"XL","condition":"hot","price":400,"discount":4,"created_at":"2020-08-14T11:40:21.000000Z","updated_at":"2020-08-14T13:26:01.000000Z"}],"pagination":{"total":1,"current_page":1,"limit":2}}
     */
    public function store(Request $request)
    {
        // TODO: use validator to avoid mass assignment vulnerability
        $res = $this->category->create($request->all());

        return response()->json([
                'success' => true,
                'data' => $res
            ]
        );
    }

    /**
     * @group  Category Management
     * Display the specified Category
     *
     * @urlParam  category required The ID of Category.
     * @param $id
     * @return JsonResponse
     * @response {"success":true,"data":{"id":11,"title":"Vans","summary":"<h1>Nice<\/h1>","photo":"https:\/\/static.nike.com\/a\/images\/t_PDP_864_v1\/f_auto,b_rgb:f5f5f5\/7c2fff38-9f89-40f1-bbcf-ffbfaab5adbc\/wio-8-road-running-shoes-S6jPM3.png","stock":1,"size":"M","condition":"default","status":"active","price":50000,"discount":5,"created_at":"2022-01-04T09:39:47.000000Z","updated_at":"2022-01-04T09:39:47.000000Z"}}
     */
    public function show($id): JsonResponse
    {
        //
        $res = $this->category->find($id);

        return response()->json([
                'success' => true,
                'data' => $res
            ]
        );
    }

    /**
     * @group  Category Management
     * Update the specified Category in storage.
     * @urlParam  category required The ID of updating Category.
     * @bodyParam  title string required Category title.
     * @bodyParam  photo string required Photo url
     * @bodyParam  summary string required Category description
     * @bodyParam  status string required enum('inactive', 'active')
     * @bodyParam  is_parent int required enum(1,0)
     * @bodyParam  parent_id int Parent category's id
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     * @response {"success":true,"message":"Update successful"}
     */
    public function update(Request $request, $id): JsonResponse
    {
        $Category = $this->category->find($id);
        $res = $this->category->where('id', $id)->update($request->all());

        return response()->json([
            'success' => true,
            'message' => "Update successful"
        ]);
    }

    /**
     * @group  Category Management
     * Remove the specified category from storage.
     * @urlParam  category required The ID of removing Category.
     * @param int $id
     * @return JsonResponse
     * @response {"success":true,"message":"Delete successful"}
     */
    public function destroy($id): JsonResponse
    {
        //
        $Category = $this->category->destroy($id);

        return response()->json([
            'success' => true,
            'message' => "Delete successful"
        ]);
    }
}
