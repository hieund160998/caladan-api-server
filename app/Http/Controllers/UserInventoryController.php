<?php
namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\UserInventory;
use App\Models\UserProfile;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;


class UserInventoryController extends Controller
{
    protected User $user;
    protected UserProfile $userProfile;
    protected UserInventory $userInventory;
    protected Item $item;

    public function __construct(User $user, UserProfile $userProfile, UserInventory $userInventory, Item $item)
    {
        $this->user = $user;
        $this->userProfile = $userProfile;
        $this->userInventory = $userInventory;
        $this->item = $item;
    }

    public function userSellInventory(Request $request): JsonResponse
    {
        $request->validate([
            'item_id' => 'required',
            'quantity' => 'required|numeric|min:1'
        ]);

        $itemDetail = $this->item->find($request->input('item_id'));
        $quantity = $request->input('quantity');

        if (empty($itemDetail)) return response()->json(['success' => false, 'message' => 'Item not found']);

        $userInventory = $this->userInventory->where('user_id', Auth::user()->id)->where('item_id', $itemDetail->id)->first();

        if (empty($userInventory)) return response()->json(['success' => false, 'message' => 'Item not found in inventory']);

        if ($quantity >= $userInventory->quantity) {
            $this->userInventory->where('id', $userInventory->id)->delete();
            $quantity = $userInventory->quantity;
        } else {
            $userInventory->quantity = $userInventory->quantity - $quantity;
            $userInventory->save();
        }

        $userProfile = $this->userProfile->where('user_id', Auth::user()->id)->first();
        $userProfile->money = $userProfile->money + $itemDetail->price * $quantity;
        $userProfile->save();

        return response()->json(['success' => true, 'message' => 'Item sold']);
    }

    public function userEquipInventory(Request $request): JsonResponse
    {
        $request->validate([
            'item_id' => 'required'
        ]);

        $itemDetail = $this->item->find($request->input('item_id'));

        if (empty($itemDetail) || !in_array($itemDetail->item_type, Item::TYPE_EQUIPABLE)) {
            return response()->json(['success' => false, 'message' => 'Invalid item !']);
        }

        $userInventory = $this->userInventory->where('user_id', Auth::user()->id)->where('item_id', $itemDetail->id)->first();

        if (empty($userInventory)) return response()->json(['success' => false, 'message' => 'Item not found in inventory']);

        $userProfile = $this->userProfile->where('user_id', Auth::user()->id)->first();

        switch ($itemDetail->item_type) {
            case Item::TYPE_HAIR : $userProfile->hair_id = $itemDetail->id; break;
            case Item::TYPE_HAT : $userProfile->hat_id = $itemDetail->id; break;
            case Item::TYPE_FACE : $userProfile->face_id = $itemDetail->id; break;
            case Item::TYPE_FACE_DECOR : $userProfile->face_decor_id = $itemDetail->id; break;
            case Item::TYPE_SUIT : $userProfile->suit_id = $itemDetail->id; break;
        }

        $userProfile->save();

        $res =  $this->userInventory->where('user_id', Auth::user()->id)->with('item')->orderBy('item_type')->orderBy('id')->get();
        return response()->json(['success' => true, 'data' => $res]);
    }
}
