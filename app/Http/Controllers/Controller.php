<?php

namespace App\Http\Controllers;

use App\Models\Permission;
use App\Models\PermissionRole;
use App\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @group  Auth Management
     * API Login
     * @bodyParam  username string required username.
     * @bodyParam  password string required password
     * @param Request $request
     * @return JsonResponse
     * @response {"status_code":200,"access_token":"15|Pww2bjqtDANWRThIocNnIaYZ0ICmfT39eTfB4yRP","token_type":"Bearer","user_info":{"id":3,"name":"test123213123","email":"harum","username":"test_1","created_at":"2022-03-15T16:31:52.000000Z","updated_at":"2022-03-15T16:31:52.000000Z","status":1}}
     */
    public function login(Request $request): \Illuminate\Http\JsonResponse
    {
        $hashPass = '';
        try {
            $request->validate([
                'username' => 'required',
                'password' => 'required'
            ]);

            $user = User::where('username', '=', $request->username)->orWhere('email', '=', $request->username)->first();

            if (empty($user)) throw new \Exception('username not found');

            if (!Hash::check($request->password, $user->password)) throw new \Exception('Invalid password.');

            $tokenResult = $user->createToken('authToken', ['player.*'])->plainTextToken;

            return response()->json([
                'status_code' => 200,
                'access_token' => $tokenResult,
                'token_type' => 'Bearer',
                'user_info' => $user
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'status_code' => 500,
                'message' => 'Error in Login',
                'error' => $error->getMessage(),
                'hash' => $hashPass
            ]);
        }
    }

    public function routeList(){
        $controllers = [];

        foreach (Route::getRoutes()->getRoutes() as $route)
        {
            $action = $route->getAction();

            if (array_key_exists('controller', $action))
            {
                // You can also use explode('@', $action['controller']); here
                // to separate the class name from the method
                if (strpos($action['controller'], '@') !== false) {
                    [$key, $value] = explode('@', $action['controller']);
                    $key = last(explode("\\", $key));
                    $controllers[$key][] = $value;
                }
            }
        }

        return response()->json($controllers);
    }

    public function updateProfile(Request $request): \Illuminate\Http\JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'password' => 'max:255',
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'tel' => 'required|max:255|min:8'
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => $validator->errors()->first()]);
        }

        $checkUniqueEmail = User::where('email', $request->input('email'))->where('id', '!=', Auth::user()->id)->first();
        if ($checkUniqueEmail) {
            return response()->json(['success' => false, 'message' => "This email is already taken"]);
        }

        if ($request->has('password') && !empty($request->input('password'))) {
            if (strlen($request->input('password') < 5)) return response()->json(['success' => false, 'message' => "The password must be at least 5 characters."]);
            $password = Hash::make($request->input('password'));
        }


        $user = User::find(Auth::user()->id);

        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->tel = $request->input('tel');
        if (isset($password)) $user->password = $password;

        $user->save();

        return response()->json([
                'success' => true,
                'message' => "Profile updated successfully"
            ]
        );
    }

    public function loginAdmin(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $request->validate([
                'email' => 'email|required',
                'password' => 'required'
            ]);

            $user = User::where('email', $request->email)->where('role', 'admin')->first();

            if (empty($user)) throw new \Exception('email not found');

            $hashPass = Hash::make($request->password);
            if (Hash::check($hashPass, $user->password)) throw new \Exception('Invalid password.');

            $tokenResult = $user->createToken('authToken', ['admin-all'])->plainTextToken;

            return response()->json([
                'status_code' => 200,
                'access_token' => $tokenResult,
                'token_type' => 'Bearer',
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'status_code' => 500,
                'message' => 'Error in Login',
                'error' => $error,
            ]);
        }
    }
}
