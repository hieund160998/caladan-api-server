<?php

namespace App\Http\Controllers;

use App\Models\FarmAnimal;
use App\Models\Item;
use App\Models\UserInventory;
use App\Models\UserProfile;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class UserPurchaseController extends Controller
{
    protected User $user;
    protected UserProfile $userProfile;
    protected UserInventory $userInventory;
    protected Item $item;
    protected FarmAnimal $farmAnimal;

    public function __construct(User $user, UserProfile $userProfile, UserInventory $userInventory, Item $item, FarmAnimal $farmAnimal)
    {
        $this->user = $user;
        $this->userProfile = $userProfile;
        $this->userInventory = $userInventory;
        $this->item = $item;
        $this->farmAnimal = $farmAnimal;
    }

    public function getListItem(Request $request): JsonResponse
    {
        $item_type = $request->input('item_type') ?? 1;

        if ($item_type == 1)
            $res = $this->item->all()->toArray();
        else
            $res = $this->item->where('item_type', $item_type)->get()->toArray();

        return response()->json(['success' => true,'data' => $res]);
    }

    public function getUserProfile(): JsonResponse
    {
        $res = $this->userProfile->where('user_id', Auth::user()->id)->with('hair')->with('hat')->with('face')->with('face_decor')->with('suit')->first();
        return response()->json(['success' => true,'data' => $res]);
    }

    public function getListUserInventory(): JsonResponse
    {
        $res =  $this->userInventory->where('user_id', Auth::user()->id)->with('item')->orderBy('item_type')->orderBy('id')->get();
        return response()->json(['success' => true, 'data' => $res]);
    }

    public function upgradeSlot(): JsonResponse
    {
        $profile = $this->userProfile->where('user_id', Auth::user()->id)->first();

        if ($profile->money >=  $profile->plot_own * 1000) {
            $profile->money -= ($profile->plot_own * 1000);
            $profile->plot_own += 3;
            $profile->save();
        } else {
            return response()->json(['success' => false, 'message' => "Not enough money to upgrade"]);
        }

        return response()->json(['success' => true, 'message' => "Upgrade complete"]);
    }


    public function userPurchase (Request $request): JsonResponse
    {
        $itemDetail = $this->item->find($request->input('itemID'));

        if (empty($itemDetail)) return response()->json(['success' => false, 'message' => 'Item not found']);

        $userInfo = $this->userProfile->where('user_id', Auth::user()->id)->first();

        if (empty($userInfo)) return response()->json(['success' => false, 'message' => 'User not found']);

        if ($userInfo->money < $itemDetail->price) return response()->json(['success' => false, 'message' => 'Not enough money']);

        if ($itemDetail->item_type == Item::TYPE_ANIMAL) {
            $this->farmAnimal->create([
                'item_id' => $itemDetail->id,
                'user_id' => Auth::user()->id,
                'product_id' => $itemDetail->product_id ?? 0,
                'start_time' => Carbon::now()->subMinutes(1),
                'growing_time' => $itemDetail->time_to_grow,
                'prefab_link' => $itemDetail->prefab_link,
                'last_fed_time' => Carbon::now()->subMinutes(1),
                'dying_time' => $itemDetail->time_to_live,
                'collectable_count' => $itemDetail->collectable_count,
                'last_collect_time' => null,
                'next_collect_time' => Carbon::now()->addMinutes($itemDetail->time_to_grow),
            ]);
        } else {
            $item = $this->userInventory->where('item_id', $itemDetail->id)->where('user_id', Auth::user()->id)->first();

            if (empty($item)) {
                $this->userInventory->create([
                    'item_id' => $itemDetail->id,
                    'item_type' => $itemDetail->item_type,
                    'user_id' => Auth::user()->id,
                    'quantity' => 1
                ]);
            } else {
                $item->quantity = $item->quantity + 1 ;
                $item->save();
            }
        }

        $userInfo->money = $userInfo->money - $itemDetail->price;
        $userInfo->save();

        $res = $this->userInventory->where('user_id', Auth::user()->id)->get();
        return response()->json(['success' => true, 'user_info'=> $userInfo, 'user_inventory' => $res]);
    }


}
