<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'title' => 'required|max:255',
            'photo' => 'required',
            'summary' => 'required|max:255',
            'stock' => 'required|integer',
            'condition' => 'required|in:default,new,hot',
            'status' => 'required|in:active,inactive',
            'price' => 'required|integer',
            'discount' => 'required|integer',
            'cat_id' => 'required|integer|exists:categories,id',
            'brand_id' => 'required|integer|exists:categories,id',
        ];
    }
}
