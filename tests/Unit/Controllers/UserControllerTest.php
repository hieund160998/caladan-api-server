<?php

namespace Tests\Unit\Controllers;
use App\Http\Controllers\UserController;
use App\Models\UserInventory;
use App\Models\UserProfile;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Mockery as m;
use Tests\TestCase;


/**
 * @covers \App\Http\Controllers\UserController
 */
class UserControllerTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->user = m::mock(User::class);
        $this->carbonMock = m::mock(Carbon::class);
        $this->requestMock = m::mock(Request::class);
        $this->userProfile = m::mock(UserProfile::class);
        $this->userInventory = m::mock(UserInventory::class);
        $this->UserControllerMock = m::mock(UserController::class, [
            $this->user,
            $this->userInventory,
            $this->userProfile
        ])->makePartial();
    }


    /**
     * @test
     * @covers \App\Http\Controllers\UserController::store()
     */
    public function test_store_user_data()
    {
        $data = [
            'username' => 'hieund',
            'password' => '12345',
            'name' => 'Hieu Nguyen',
            'email' => 'hieu123@gmail.com',
            'tel' => '0899898128',
        ];

        $this->requestMock->shouldReceive('input')->andReturn();
        $this->requestMock->shouldReceive('all');
        $this->requestMock->shouldReceive('merge');
        $this->user->shouldReceive('create')->andReturn([]);
        $this->userProfile->shouldReceive('create')->andReturn([]);
        $this->userInventory->shouldReceive('create')->andReturn([]);

        $res = $this->UserControllerMock->store($this->requestMock);

        $this->assertInstanceOf(JsonResponse::class, $res);
    }
}
