<?php

namespace Tests\Unit\Controllers;

use App\Http\Controllers\UserPurchaseController;
use App\Models\UserInventory;
use App\Models\UserProfile;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Tests\TestCase;
use Mockery as m;

/**
 * @covers \App\Http\Controllers\UserPurchaseController
 */
class UserPurchaseControllerTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->user = m::mock(User::class);
        $this->carbonMock = m::mock(Carbon::class);
        $this->requestMock = m::mock(Request::class);
        $this->userProfile = m::mock(UserProfile::class);
        $this->userInventory = m::mock(UserInventory::class);
        $this->UserPurchaseControllerMock = m::mock(UserPurchaseController::class, [
            $this->user,
            $this->userInventory,
            $this->userProfile
        ])->makePartial();
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Controller::getItemList()
     */
    public function test_get_item_list()
    {
        $this->requestMock->shouldReceive('validate')->andReturn(true);

        $this->requestMock->shouldReceive('input')->andReturn();
        $this->requestMock->shouldReceive('all');
        $this->requestMock->shouldReceive('merge');
        $this->user->shouldReceive('where')->andReturn([]);
        $this->user->shouldReceive('orWhere')->andReturn([]);
        $this->user->shouldReceive('first')->andReturn([]);
        $this->user->shouldReceive('createToken')->andReturn([]);

        $res = $this->UserControllerMock->store($this->requestMock);

        $this->assertInstanceOf(JsonResponse::class, $res);
    }
}
