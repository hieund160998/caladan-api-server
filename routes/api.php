<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Requests $request) {
//    return $request->user();
//});

Route::get('/list', 'Controller@routeList');
Route::post('/login', 'Controller@login');
Route::post('/admin-login', 'Controller@loginAdmin');

Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('/me', function (Request $request) {
        return response()->json([
            'success' => true,
            'data' => \Illuminate\Support\Facades\Auth::user()
        ]);
    });

    Route::post('/update-profile', 'Controller@updateProfile');

    Route::get('/animal-list', 'FarmInventoryController@getListAnimal');
    Route::post('/animal-feed', 'FarmInventoryController@feedAnimal');
    Route::post('/animal-collect', 'FarmInventoryController@collectAnimalProduct');


    Route::get('/plant-list', 'FarmInventoryController@getListPlant');
    Route::post('/plot-plant', 'FarmInventoryController@plantSeed');
    Route::post('/water-plant', 'FarmInventoryController@plantWater');
    Route::post('/remove-plant', 'FarmInventoryController@plantRemove');
    Route::post('/harvest-plant', 'FarmInventoryController@plantHarvest');

    Route::get('/item-list', 'UserPurchaseController@getListItem');
    Route::get('/user-info', 'UserPurchaseController@getUserProfile');
    Route::get('/user-inventory', 'UserPurchaseController@getListUserInventory');

    Route::post('/sell-inventory', 'UserInventoryController@userSellInventory');
    Route::post('/equip-inventory', 'UserInventoryController@userEquipInventory');

    Route::post('/user-purchase', 'UserPurchaseController@userPurchase');
    Route::get('/upgrade-slot', 'UserPurchaseController@upgradeSlot');

    Route::middleware(['abilities:permission_access'])->group(function () {

    });
});

Route::middleware(['cors'])->group(function () {
 //   Route::get('/item-list', 'UserPurchaseController@getListItem');


    Route::resource('service-type', 'ServiceTypeController');

    Route::resource('service', 'ServiceController');

    Route::resource('construction', 'ConstructionController');

    Route::resource('user', 'UserController');

    Route::resource('role', 'RoleController');

    Route::resource('permission', 'PermissionController');
});


Route::middleware(['auth:sanctum','abilities:admin-all'])->group(function () {
    Route::get('/admin', function (Request $request) {
        return response()->json([
            'success' => true,
            'message' => 'Admin Login',
        ]);
    });

//    Route::resource('category', 'CategoryController');
});
